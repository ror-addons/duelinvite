DuelInvite = {}

DuelInvite.InteractionMenuHooked = false

function DuelInvite.OnInitialize()
	-- set hook and we're done
	DuelInvite.SetInteractionMenuHook(true)
	TextLogAddEntry("Chat", 0, L"DuelInvite initialized")	
end

function DuelInvite.SetInteractionMenuHook(enable)
	if enable and not DuelInvite.InteractionMenuHooked then
		--hook
		DuelInvite.AddInteractionMenuItemsHook = PlayerMenuWindow.AddInteractionMenuItems
		PlayerMenuWindow.AddInteractionMenuItems = DuelInvite.AddInteractionMenuItems
		DuelInvite.InteractionMenuHooked = true
	elseif not enable and DuelInvite.InteractionMenuHooked then
		--unhook
		PlayerMenuWindow.AddInteractionMenuItems = DuelInvite.AddInteractionMenuItemsHook
		DuelInvite.InteractionMenuHooked = false
	end
end

function DuelInvite.AddInteractionMenuItems( targetSelf )
	DuelInvite.AddInteractionMenuItemsHook(targetSelf)
    local targetGameAction = EA_Window_ContextMenu.GameActionData( GameData.PlayerActions.SET_TARGET, 0,  PlayerMenuWindow.curPlayer.name )

    local disableAllButTarget = (targetSelf == true)
    -- Duel the Player <icon00076>
    EA_Window_ContextMenu.AddMenuItem( L"Duel", DuelInvite.OnDuel, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1, targetGameAction )

end

function DuelInvite.OnDuel()    
	--d("duelling")
    -- if( ButtonGetDisabledFlag(SystemData.ActiveWindow.Name ) == true ) then
    --     return
    -- end

    -- SystemData.UserInput.ChatText = 
    SendChatText( L"/duel "..PlayerMenuWindow.curPlayer.name, L"" )
    PlayerMenuWindow.Done()
    
    -- BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
    -- PlayerMenuWindow.Done()
end


-- function PlayerMenuWindow.AddInteractionMenuItems( targetSelf )

--     local targetGameAction = EA_Window_ContextMenu.GameActionData( GameData.PlayerActions.SET_TARGET, 0,  PlayerMenuWindow.curPlayer.name )
 
--     EA_Window_ContextMenu.AddMenuDivider( EA_Window_ContextMenu.CONTEXT_MENU_1 )
    
--     local disableAllButTarget = (targetSelf == true)

--     -- Talk to the Player
--     EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_PLAYER_MENU_TALK ), PlayerMenuWindow.OnTalk, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1 )

--     -- Target the Player
--     EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_PLAYER_MENU_TARGET ), PlayerMenuWindow.OnTarget, false, true, EA_Window_ContextMenu.CONTEXT_MENU_1, targetGameAction )

--     -- Assist the Player
--     EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_ASSIST ), PlayerMenuWindow.OnAssist, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1, nil )

--     -- Trade the the Player
--     EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_PLAYER_MENU_TRADE ), PlayerMenuWindow.OnTrade, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1, targetGameAction )

--     -- Inspect the Player
--     EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_PLAYER_MENU_INSPECT ), PlayerMenuWindow.OnInspect, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1, targetGameAction )

--     -- Follow the Player
--     EA_Window_ContextMenu.AddMenuItem( GetString( StringTables.Default.LABEL_PLAYER_MENU_FOLLOW ), PlayerMenuWindow.OnFollow, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1, targetGameAction )

--     EA_Window_ContextMenu.AddMenuItem( L"Duel", Duel.OnDuel, disableAllButTarget, true, EA_Window_ContextMenu.CONTEXT_MENU_1, targetGameAction )
-- end