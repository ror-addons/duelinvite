<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="DuelInvite" version="1.0.1" date="02-Aug-17">
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
		<Author name="anon" />
        <Description text="Adds duel option to RClick context menu" />
        <Dependencies />
        <Files>
            <File name="DuelInvite.lua" />
        </Files>
        <OnInitialize>
             <CallFunction name="DuelInvite.OnInitialize" />
        </OnInitialize>

        <OnShutdown />
    </UiMod>
</ModuleFile>